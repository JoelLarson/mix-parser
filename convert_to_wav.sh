#!/bin/bash

display_usage() {
    echo "This script will convert mp3 to wav with the same filename."
	echo -e "\nUsage:\n./convert_to_wav.sh mix_file.mp3 \n"
}

if [ $# -eq 0 ]; then
    display_usage
    exit 1
fi

NEW_FILE="$(echo $1 | cut -d"." -f1).wav"

echo "Converting $1 to $NEW_FILE..."

sox $1 $NEW_FILE
