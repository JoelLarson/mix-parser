# Mix Parser

Parses all yo mixes

# Requirements

```
brew install chromaprint ffmpeg sox
```

# How to Use

* Download MP3 from SoundCloud (or another mix source)
* Run mp3 file through `./convert_to_wav.sh mix_file.mp3` to get WAV file
* Run `./breakdown.sh mix_file.wav` to split mix into multiple chunks in `./mix_file/*`
* Run `./determine_track mix_file` to iterate over files, fingerprint, then check for results.

# TODO
* Fingerprinting works, but checking for files does not return results currently. (adjust chunksize, songs?)
* Investigate hosting and filling internal web service that does fingerprint checking https://bitbucket.org/acoustid/acoustid-server