#!/bin/bash

display_usage() {
    echo "This script chunks the WAV file into smaller sections into a folder named after the base filename."
    echo -e "\nUsage:\n./breakdown.sh mix_file.wav \n" 
}

if [  $# -eq 0 ]; then 
    display_usage
    exit 1
fi 

echo "Determining duration of track..."

FILE_NAME=$1
RAW_NAME=$(echo $FILE_NAME | cut -d'.' -f1)

# in seconds
CHUNK_SIZE=10
DURATION=$(sox deadmau5_mix.wav -n stat 2>&1 | sed -n 's#^Length (seconds):[^0-9]*\([0-9.]*\)$#\1#p' | awk '{print int($1+0.5)}')

mkdir -p ./$RAW_NAME

echo "Splitting files..."
ruby -e "(0..$DURATION).step($CHUNK_SIZE) { |x| system \"ffmpeg -loglevel panic -hide_banner -ss #{x} -i ${FILE_NAME} -c copy -t $CHUNK_SIZE './${RAW_NAME}/#{x}-${RAW_NAME}.wav'\"}"

echo "Boop! Grab a beer. You deserve it."
