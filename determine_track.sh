#!/bin/bash

display_usage() {
    echo "This script checks against acoustid.org after fingerprinting each file in the given directory."
    echo -e "\nUsage:\n./determine_track.sh mix_file \n"
}

if [  $# -le 1 ]; then 
    display_usage
    exit 1
fi 

dir=$1

for filename in ./${dir}/*; do
  echo "$filename"
  echo ""
  FINGER_PRINT=$(fpcalc -length 20 "${filename}" | grep FINGERPRINT | cut -d'=' -f2)
  DURATION=10
  curl -s -X GET "http://api.acoustid.org/v2/lookup?client=EJSkirGNlz&duration=${DURATION}&fingerprint=${FINGER_PRINT}"
  echo ""
  echo ""
  sleep 1
done

#curl -s -X GET "http://api.acoustid.org/v2/lookup?client=EJSkirGNlz&duration=${DURATION}&fingerprint=${FINGER_PRINT}"

#http://api.acoustid.org/v2/lookup?client=EJSkirGNlz&duration=140&fingerprint=
